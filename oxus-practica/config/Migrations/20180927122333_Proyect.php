<?php
use Migrations\AbstractMigration;

class Proyect extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('proyect');
        $table->addColumn('name', 'string', [
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('cat_id', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('state_id', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('date_star', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('date_finished', 'string', [
            'default' => null,
            'null' => false,
        ]);
        
        $table->create();
    }
}
