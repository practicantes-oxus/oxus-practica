<?php
use Migrations\AbstractMigration;

class Users extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('email', 'string', [
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('password', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_name_pat', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_name_mat', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('role_id', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
