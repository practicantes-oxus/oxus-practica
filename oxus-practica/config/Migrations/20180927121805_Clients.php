<?php
use Migrations\AbstractMigration;

class Clients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('clients');
        $table->addColumn('rut', 'string', [
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_name_pat', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_name_mat', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('number_phone', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('email', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('address', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('entreprise', 'string', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
