<div class="login-box border border-success">
  <div class="login-logo ">
  <h1><a href=""><?= $this->Html->image('logo.png');?></a></h1>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

	<?= $this->Form->create() ?>
	<fieldset>
      <div class="form-group has-feedback">
	  <?= $this->Form->input('email', ['class' => 'form-control input-lg', 'placeholder' => 'Correo electrónico', 'label' => false, 'require']);?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
	  <?= $this->Form->input('password', ['class' => 'form-control input-lg', 'placeholder' => 'Contraseña', 'label' => false, 'require']);?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?= $this->Form->button('Acceder', ['class' => 'btn btn-lg btn-success btn-block'])?>
		</div>
		
        <!-- /.col -->
	  </div>
	  </fieldset>
	  <?= $this->Form->end() ?>
    <!-- /.social-auth-links -->

    <a href="#">¿Olvidaste la contaseña?</a><br>

  </div>
  <!-- /.login-box-body -->
</div>