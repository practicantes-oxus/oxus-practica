<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name Pat') ?></th>
            <td><?= h($user->last_name_pat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name Mat') ?></th>
            <td><?= h($user->last_name_mat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Roles Id') ?></th>
            <td><?= $this->Number->format($user->roles_id) ?></td>
        </tr>
    </table>
</div>
