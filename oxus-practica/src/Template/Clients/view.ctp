<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div class="clients view large-9 medium-8 columns content">
    <h3><?= h($client->name)?> <?=($client->last_name_pat) ?> <?=($client->last_name_mat)?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Rut') ?></th>
            <td><?= h($client->rut) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($client->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Paterno') ?></th>
            <td><?= h($client->last_name_pat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido Materno') ?></th>
            <td><?= h($client->last_name_mat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero de Telefono') ?></th>
            <td><?= h($client->number_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($client->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Direccion') ?></th>
            <td><?= h($client->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $client->has('enterprise') ? $this->Html->link($client->enterprise->name, ['controller' => 'Enterprise', 'action' => 'view', $client->enterprise->id]) : '' ?></td>
        </tr>
        
        <tr>
            <th scope="row"><?= __('ingresado') ?></th>
            <td><?= h($client->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ultima Modificacion') ?></th>
            <td><?= h($client->modified) ?></td>
        </tr>
    </table>
</div>
