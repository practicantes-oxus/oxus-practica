<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div class="clients form large-9 medium-8 columns content">
    <?= $this->Form->create($client) ?>
    <fieldset>
        <legend><?= __('Add Client') ?></legend>
        <?php
            echo $this->Form->control('rut');
            echo $this->Form->control('name');
            echo $this->Form->control('last_name_pat');
            echo $this->Form->control('last_name_mat');
            echo $this->Form->control('number_phone');
            echo $this->Form->control('email');
            echo $this->Form->control('address');
            echo $this->Form->control('enterprise_id', ['options' => $enterprise]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
