<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Proyect $proyect
 */
?>
<div class="proyects view large-9 medium-8 columns content">
    <h3><?= h($proyect->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($proyect->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= $proyect->has('client') ? $this->Html->link($proyect->client->id, ['controller' => 'Clients', 'action' => 'view', $proyect->client->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $proyect->has('user') ? $this->Html->link($proyect->user->name, ['controller' => 'Users', 'action' => 'view', $proyect->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categoria del proyecto') ?></th>
            <td><?= $proyect->has('proyect_category') ? $this->Html->link($proyect->proyect_category->name, ['controller' => 'ProyectCategories', 'action' => 'view', $proyect->proyect_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado del proyecto') ?></th>
            <td><?= $proyect->has('proyect_state') ? $this->Html->link($proyect->proyect_state->name, ['controller' => 'ProyectStates', 'action' => 'view', $proyect->proyect_state->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha inico') ?></th>
            <td><?= h($proyect->date_start) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de termino') ?></th>
            <td><?= h($proyect->date_finished) ?></td>
        </tr>
    </table>
</div>
