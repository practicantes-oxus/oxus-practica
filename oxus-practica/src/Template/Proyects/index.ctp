<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Proyect[]|\Cake\Collection\CollectionInterface $proyects
 */
?>
<div class="proyects index large-9 medium-8 columns content">
    <h3><?= __('Proyectos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Cliente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Usuario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Categoria de proyecto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado de proyecto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Fecha de inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Fecha de termino') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proyects as $proyect): ?>
            <tr>
                <td><?= $this->Number->format($proyect->id) ?></td>
                <td><?= h($proyect->name) ?></td>
                <td><?= $proyect->has('client') ? $this->Html->link($proyect->client->id, ['controller' => 'Clients', 'action' => 'view', $proyect->client->id]) : '' ?></td>
                <td><?= $proyect->has('user') ? $this->Html->link($proyect->user->name, ['controller' => 'Users', 'action' => 'view', $proyect->user->id]) : '' ?></td>
                <td><?= $proyect->has('proyect_category') ? $this->Html->link($proyect->proyect_category->name, ['controller' => 'ProyectCategories', 'action' => 'view', $proyect->proyect_category->id]) : '' ?></td>
                <td><?= $proyect->has('proyect_state') ? $this->Html->link($proyect->proyect_state->name, ['controller' => 'ProyectStates', 'action' => 'view', $proyect->proyect_state->id]) : '' ?></td>
                <td><?= h($proyect->date_start) ?></td>
                <td><?= h($proyect->date_finished) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $proyect->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $proyect->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $proyect->id], ['confirm' => __('Are you sure you want to delete # {0}?', $proyect->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primera ')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultima') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
