<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Proyect $proyect
 */
?>
<div class="proyects form large-9 medium-8 columns content">
    <?= $this->Form->create($proyect) ?>
    <fieldset>
        <legend><?= __('Editar Proyectos') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('clients_id', ['options' => $clients]);
            echo $this->Form->control('users_id', ['options' => $users]);
            echo $this->Form->control('proyect_categories_id', ['options' => $proyectCategories]);
            echo $this->Form->control('proyect_states_id', ['options' => $proyectStates]);
            echo $this->Form->control('date_start');
            echo $this->Form->control('date_finished');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
