<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $rut
 * @property string $name
 * @property string $last_name_pat
 * @property string $last_name_mat
 * @property string $number_phone
 * @property string $email
 * @property string $address
 * @property int $enterprise_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Enterprise $enterprise
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rut' => true,
        'name' => true,
        'last_name_pat' => true,
        'last_name_mat' => true,
        'number_phone' => true,
        'email' => true,
        'address' => true,
        'enterprise_id' => true,
        'created' => true,
        'modified' => true,
        'enterprise' => true
    ];

    /*Para poder hashear la contraseña utilizar siguente codigo:

            protected function _setPassword($value)
            {
                $hasher= new DefaultPasswordHasher();
                return $hasher->hash($value);
            }
    
    y tambien definirla arribacon el siguente codigo:

             use Cake\Auth\DefaultPasswordHasher;
    */

    protected function _setPassword($value)
    {
        $hasher= new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
}
