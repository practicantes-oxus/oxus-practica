<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Proyect Entity
 *
 * @property int $id
 * @property string $name
 * @property int $clients_id
 * @property int $users_id
 * @property int $proyect_categories_id
 * @property int $proyect_states_id
 * @property \Cake\I18n\FrozenDate $date_start
 * @property \Cake\I18n\FrozenDate $date_finished
 *
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ProyectCategory $proyect_category
 * @property \App\Model\Entity\ProyectState $proyect_state
 */
class Proyect extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'clients_id' => true,
        'users_id' => true,
        'proyect_categories_id' => true,
        'proyect_states_id' => true,
        'date_start' => true,
        'date_finished' => true,
        'client' => true,
        'user' => true,
        'proyect_category' => true,
        'proyect_state' => true
    ];
}
