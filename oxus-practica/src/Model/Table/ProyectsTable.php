<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Proyects Model
 *
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ProyectCategoriesTable|\Cake\ORM\Association\BelongsTo $ProyectCategories
 * @property \App\Model\Table\ProyectStatesTable|\Cake\ORM\Association\BelongsTo $ProyectStates
 *
 * @method \App\Model\Entity\Proyect get($primaryKey, $options = [])
 * @method \App\Model\Entity\Proyect newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Proyect[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Proyect|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Proyect|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Proyect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Proyect[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Proyect findOrCreate($search, callable $callback = null, $options = [])
 */
class ProyectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('proyects');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Clients', [
            'foreignKey' => 'clients_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProyectCategories', [
            'foreignKey' => 'proyect_categories_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProyectStates', [
            'foreignKey' => 'proyect_states_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->date('date_start')
            ->requirePresence('date_start', 'create')
            ->notEmpty('date_start');

        $validator
            ->date('date_finished')
            ->requirePresence('date_finished', 'create')
            ->notEmpty('date_finished');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['clients_id'], 'Clients'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));
        $rules->add($rules->existsIn(['proyect_categories_id'], 'ProyectCategories'));
        $rules->add($rules->existsIn(['proyect_states_id'], 'ProyectStates'));

        return $rules;
    }
}
