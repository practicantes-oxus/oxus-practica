<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Proyects Controller
 *
 * @property \App\Model\Table\ProyectsTable $Proyects
 *
 * @method \App\Model\Entity\Proyect[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProyectsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients', 'Users', 'ProyectCategories', 'ProyectStates']
        ];
        $proyects = $this->paginate($this->Proyects);

        $this->set(compact('proyects'));
    }

    /**
     * View method
     *
     * @param string|null $id Proyect id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $proyect = $this->Proyects->get($id, [
            'contain' => ['Clients', 'Users', 'ProyectCategories', 'ProyectStates']
        ]);

        $this->set('proyect', $proyect);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $proyect = $this->Proyects->newEntity();
        if ($this->request->is('post')) {
            $proyect = $this->Proyects->patchEntity($proyect, $this->request->getData());
            if ($this->Proyects->save($proyect)) {
                $this->Flash->success(__('The proyect has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The proyect could not be saved. Please, try again.'));
        }
        $clients = $this->Proyects->Clients->find('list', ['limit' => 200]);
        $users = $this->Proyects->Users->find('list', ['limit' => 200]);
        $proyectCategories = $this->Proyects->ProyectCategories->find('list', ['limit' => 200]);
        $proyectStates = $this->Proyects->ProyectStates->find('list', ['limit' => 200]);
        $this->set(compact('proyect', 'clients', 'users', 'proyectCategories', 'proyectStates'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Proyect id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $proyect = $this->Proyects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $proyect = $this->Proyects->patchEntity($proyect, $this->request->getData());
            if ($this->Proyects->save($proyect)) {
                $this->Flash->success(__('The proyect has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The proyect could not be saved. Please, try again.'));
        }
        $clients = $this->Proyects->Clients->find('list', ['limit' => 200]);
        $users = $this->Proyects->Users->find('list', ['limit' => 200]);
        $proyectCategories = $this->Proyects->ProyectCategories->find('list', ['limit' => 200]);
        $proyectStates = $this->Proyects->ProyectStates->find('list', ['limit' => 200]);
        $this->set(compact('proyect', 'clients', 'users', 'proyectCategories', 'proyectStates'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Proyect id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $proyect = $this->Proyects->get($id);
        if ($this->Proyects->delete($proyect)) {
            $this->Flash->success(__('The proyect has been deleted.'));
        } else {
            $this->Flash->error(__('The proyect could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
